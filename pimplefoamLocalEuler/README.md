# README #

## How to Use the Solver and test cases ##
1. Create the following directories if you don't have already.

Run directory - your case directories are usually here. It is the same as $FOAM_RUN
`mkdir -p $WM_PROJECT_USER_DIR/run`
Source code for your custom directory goes here. Its
`mkdir -p $WM_PROJECT_USER_DIR/src`

Compiled binaries and libraries go into the following directories.
`mkdir -p $FOAM_USER_APPBIN`
`mkdir -p $FOAM_USER_LIBBIN`

## 
2. Copy the solver directory from the bitbucket repository into the "src" directory as created above.
3. Using terminal, go into this directory of solver and execute `wmake` command. If it succeeds then your solver is ready.
4. Now copy the pitzdailyLTS solver into your "run" directory and run the case using `pimpleFoamUser`