# README for the project

## Repos for OpenFOAM 2.3.1
### `bslearsm2.3.1`
This contains the compressible modified version(to make it work in OF2.3.1) of the BSL-EARSM model of Wallin and Johansson as implemented by :

([BSL-EARSM/bitbucket](https://bitbucket.org/akidess/bsl-earsm/src))

or

([BSL-EARSM/Gitlab](https://gitlab.com/ejp/BSL-EARSM))


Both of the above repos are from the same creator and are developed for OF1.7. Refer to the links for more details.

### `bslearsm_ico2.3.1`
The incompressible verison of the above model.

### `kOmegaBSL2.3.1`
kOmegaBSL model derived from SST model of OpenFOAM. Only minor modifications were required to be done.

### `parabolicVelocityFoamExtend2.3.1`
Inlet velocit profile from foam-extend.

### `powerLawTurbulentVelocity2.3.1`
This is not complete. Should not be used as of now.


### `wallHeatFluxAWTcfx2.3.1`
This implements the utility to calculate wall Heat flux for Wall Function meshes. The theory is taken from CFX solver theory guide. This is equivalent to the Automatic Wall Treatment for omega based models in CFX.

## Repos for OpenFOAM 4.1
### `bslearsm4.1`
This contains the compressible modified version(to make it work in OF4.1) of the model for 2.3.1.


### `wallHeatFluxAWTcfx4.1`
This implements the utility to calculate wall Heat flux for Wall Function meshes. The theory is taken from CFX solver theory guide. This is equivalent to the Automatic Wall Treatment for omega based models in CFX.

